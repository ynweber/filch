# all this was necessary to give me a case sensitive predicate!!!
# stolen from https://github.com/activerecord-hackery/ransack/issues/699
# {{
Ransack.configure do |config|
  config.add_predicate(
    'like',
    arel_predicate: 's_matches',
    formatter: proc { |v| "%#{v}%" },
    type: :string
  )
end

module Arel
  module Nodes
    class Matches < Binary
      attr_reader :escape
      attr_accessor :case_sensitive

      def initialize(left, right, escape = nil, case_sensitive = false)
        super(left, right)
        @escape = escape && Nodes.build_quoted(escape)
        @case_sensitive = case_sensitive
      end
    end

    class DoesNotMatch < Matches; end
  end
end

module Arel
  module Predications
    def s_matches(other, escape = nil, case_sensitive = true)
      Nodes::Matches.new self, quoted_node(other), escape, true
    end
  end
end

module Arel
  module Visitors
    class PostgreSQL < Arel::Visitors::ToSql
      private

      def visit_Arel_Nodes_Matches o, collector
        op = o.case_sensitive ? ' LIKE ' : ' ILIKE '
        collector = infix_value o, collector, op
        if o.escape
          collector << ' ESCAPE '
          visit o.escape, collector
        else
          collector
        end
      end
    end
  end
end
# }}

# Array Contains
#  {{{
# https://github.com/activerecord-hackery/ransack/issues/321
module Arel
  module Predications
    def array_cont(other)
      Nodes::Equality.new(Nodes.build_quoted(other, self), Nodes::NamedFunction.new('ANY', [self]))
    end
  end
end

Ransack.configure do |config|
  config.add_predicate :array_cont, arel_predicate: :array_cont
end
# }}}

# Array Not Contains
#  {{{
# https://github.com/activerecord-hackery/ransack/issues/321
module Arel
  module Predications
    def array_not_cont(other)
      Nodes::DoesNotMatch.new(Nodes.build_quoted(other, self), Nodes::NamedFunction.new('ALL', [self]))
    end
  end
end

Ransack.configure do |config|
  config.add_predicate :array_not_cont, arel_predicate: :array_not_cont
end
# }}}

#
# vim:foldmethod=marker
