require 'filch/version'
require 'filch/engine'
require 'filch/error'
require 'filch/datalist'
require 'filch/ransack_plus'
require 'filch/model_find'

# = The Filch Gem
# the Filch Gem is entirely based on the
# ransack[https://github.com/activerecord-hackery/ransack] gem
# Filch allows for easy advanced form building.
#
# ==Features
# Filch::Templates:: The Filch::Templates object is used when generating
#                    the form(s).
# Filch::Datalist:: Text fields are linked to a <datalist>.
#                   Uses javascript to update the list with
#                   Filch::Datalist.quick
# Filch::RansackPlus:: Certain methods I couldn't do with Ransack/Arel.
module Filch
  def self.templates(model)
    Templates.new(model)
  end

  # calls the Filch::Datalist.quick method.
  # <i>model_name</i> is a String representing the name of the model.
  # <i>attr</i> is a String representing the attribute to query.
  # <i>params</i> is the form's params.
  #
  # returns an Array 'plucked' from the results
  #
  # this method is usually called internally, from javascript
  def self.datalist(model_name, attr, params)
    Datalist.quick(model(model_name), attr, params)
  end

  # Filch::RansackPlus.
  # recieves the form <i>params<i>, for the 'plus' methods
  # <i>q</i> is the form object.
  # returns an array of ransacked results
  def self.ransack_plus(params, q)
    RansackPlus.new(params, q)
  end

  # model_find should be a String representing the name of a model.
  # returns a model.
  # If model_find is nil, or the model is not found, return the first model.
  def self.model(model_find = nil)
    ModelFind.find(model_find)
  end

  # = Templates
  # Builds a hash. keys ares are an array template_keys
  # , values are Filch::Template.
  #   @templates[template_key] = Filch::Template.new(@model, template_key)
  # template_keys can be defined in the model
  #   class Foo < ApplicationRecord
  #     def self.filch_template_keys
  #       %i[all basic]
  #     end
  #   end
  class Templates
    def initialize(model)
      @model = model
      build_templates!
    end
    attr_reader :templates, :model

    def build_templates!
      @templates = {}
      @templates.default = Template.new(@model, 'default')
      template_keys.each do |template|
        @templates[template] = Template.new(@model, template)
      end
    end

    # will generate forms based on the list of templates in the Array
    # <i>YourModel.template_keys</i>.
    # defaults to ['all']
    def template_keys
      @model.methods.include?(:template_keys) ? @model.template_keys || ['all'] : ['all']
    end
  end

  # The Template Object
  # defines a template
  # recieves a model Object, and a String representing the name of the template.
  # returns a new object with the following attributes:
  # [associations_keys] an Array of Strings representing the names of the
  #                     the Filch::Associations to be queryed.
  # [associations] a Hash of Strings representing the predicates to be queryed.
  # [attribute_keys] same as association_keys, for the the Filch::Attributes
  # [attributes] same as associations, for the Filch::Attributes.
  # [scope_keys] same as association_keys, for the Filch::Scopes
  # [scopes] same as associations, for the Filch::Scopes
  class Template
    def initialize(model, template)
      @model = model
      @template = template
      assocs = Associations.new(model)
      @associations = assocs.associations[template]
      @associations_keys = assocs.associations_keys[template]
      @scopes = Scopes.new(model).scopes[template]
      attrs = Attributes.new(model)
      @attributes = attrs.attributes[template]
      @attributes_keys = attrs.attributes_keys[template]
    end
    attr_reader :associations, :scopes, :attributes, :attributes_keys,
                :associations_keys

    def default_values
      @model.methods.include?(:filch_defaults) ? @model.filch_defaults[@template] : {}
    end

    def options
      @model.methods.include?(:filch_options) ? @model.filch_options[@template] : {}
    end
  end

  # associations_keys, to determine which associations to query &
  # #associations, to determine which predicates to query on those associations.
  class Associations
    def initialize(model)
      @model = model
      @ransackable_associations = @model ? @model.ransackable_associations : []
    end

    # the associated models to be queryed.
    # associations_keys can be defined in the model
    #   class Foo < ApplicationRecord
    #     has_many :employees
    #     has_many :pay_stubs
    #     def self.filch_assoc_keys
    #       %i[employess pay_stubs]
    #     end
    #   end
    #
    # defaults to all ransackable_associtations
    def associations_keys
      assoc_keys =
        @model.respond_to?(:filch_assoc_keys) ? @model.filch_assoc_keys : {}
      assoc_keys.default = @ransackable_associations
      assoc_keys[:all] = @ransackable_associations
      assoc_keys
    end

    # a hash representing all the predicates to be queryed for a given
    # association.
    # #associations can be defined in the model
    #   class Foo < ApplicationRecord
    #     has_many :employees
    #     has_many :pay_stubs
    #     def self.filch_associations
    #       {
    #         basic_template: {
    #           employee: {
    #             name: %w[eq cont]
    #           },
    #           pay_stub: {
    #             year: %w[eq gt]
    #           }
    #         }
    #       }
    #     end
    #   end
    #
    # defaults to all ransackable_attributes for a given association with a
    # value of ['eq']
    def associations
      assoc_hash =
        @model.respond_to?(:filch_associations) ? @model.filch_associations : {}
      assoc_hash.default = all
      assoc_hash[:all] = all
      assoc_hash
    end

    # TODO: specialize defaults to class types
    # using class_eval(assoc.capitalize).columns
    # doesnt work if belongs_to is not the same name as class
    def all
      all_hash = {}
      @ransackable_associations.each do |assoc|
        all_hash[assoc] = {}
        all_hash[assoc].default = ['eq']
        next unless Object.const_defined?(assoc.capitalize)
        Object.const_get(assoc.capitalize).ransackable_attributes.each do |attr|
          all_hash[assoc][attr] = ['eq']
        end
        # assoc_class_name = assoc.capitalize
      end
      all_hash
    end
  end

  # similar to Filch::Associations, except doesn't use predicates, and
  # ransackable_scopes <b>must be</b> defined.
  #   class Foo < ApplicationRecord
  #     scope :managers, -> { where(position: 'Manager') }
  #     scope :store_managers, -> { where(position: 'StoreManager') }
  #     def self.ransackable_scopes
  #       %w[managers, store_managers]
  #     end
  #   end
  #
  # defaults to <i>ransackable_scopes</i>
  class Scopes
    def initialize(model)
      @model = model
    end

    # can be defined on the model
    #   class Foo < ApplicationRecord
    #   def self.filch_scopes
    #     %w[managers, store_managers]
    def scopes
      scopes_hash = @model.respond_to?(:filch_scopes) ? @model.filch_scopes : {}
      scopes_hash.default = default
      scopes_hash[:all] = @model ? @model.ransackable_scopes : []
      scopes_hash
    end

    def default
      default_hash = {}
      default_hash
    end
  end

  # similar to Filch::Associations
  class Attributes
    def initialize(model)
      @model = model
      @ransackable_attributes = @model ? @model.ransackable_attributes : []
    end

    # defaults to <i>Foo.ransackable_attributes</i>
    def attributes_keys
      attributes_keys =
        @model.respond_to?(:filch_attrs_keys) ? @model.filch_attrs_keys : {}
      attributes_keys.default = @ransackable_attributes
      attributes_keys[:all] = @ransackable_attributes
      attributes_keys
    end

    def attributes
      attributes_hash = if @model && @model.methods.include?(:filch_attributes)
                          @model.filch_attributes
                        else
                          {}
                        end
      attributes_hash.default = default
      attributes_hash.each_value { |template_h| template_h.default = ['eq'] }
      attributes_hash[:all] = all
      attributes_hash
    end

    def all
      all_hash = {}
      @ransackable_attributes.each do |attr|
        all_hash[attr] = types[
          @model.columns.select { |col| col.name == attr }.first.type
        ]
      end
      all_hash
    end

    def default
      default_hash = Hash.new(['eq'])
      default_hash
    end

    # TODO: setup defaults based on column_type
    def types
      types_hash = Hash.new(['eq'])
      types_hash[:integer] = %w[eq lt gt]
      types_hash[:string] = %w[null eq start matches]
      types_hash[:text] = types_hash['string']
      types_hash[:boolean] = %w[null]
      types_hash
    end
  end
end
