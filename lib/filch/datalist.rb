module Filch
  # Ransack for a datalist
  class Datalist
    def datalist
      yield self
    end

    def self.quick(model, column, params)
      col = model.columns_hash[column]
      attrs = model.ransackable_attributes
      q = if attrs.include?(column) && col.default == '{}'
            arrayify(model, params, column)
          else
            model.ransack(params).result
                 .group("#{model.name.pluralize}.#{column}")
                 .limit(5).pluck(column)
          end
      q
    end

    # unnest a PostGress Array.
    def self.arrayify(model, params, column)
      model.ransack(params).result.group(Arel.sql('unnest')).limit(5)
           .pluck(Arel.sql("unnest(#{column})"))
    end
  end
end
