module Filch
  class ModelFind
    def self.find(find_model = nil)
      models = all_models
      found_model = models.select { |model| model.name == find_model }.first
      found_model || models.first if models.any? || ApplicationRecord.new
    end

    def self.all_models
      Rails.application.eager_load!
      ActiveRecord::Base.descendants
      ApplicationRecord.descendants
    end
  end
end
