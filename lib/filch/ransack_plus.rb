module Filch
  # Ransack for results, and then manipulate data as necessary.
  class RansackPlus
    def initialize(model, params)
      @params = params
      # @q = q
      puts 'setting limit'
      set_limit! if @params
      puts 'limit is set'
      @q = model.ransack(@params)
      puts '@q is set'
      @result = ransack_result
      puts 'plus done'
    end
    attr_reader :q, :result

    # get the results!
    def ransack_result
      return [] unless @params
      @words = @q.result
      puts '@words is set'
      # distinctify!
      @words
    end

    # adjust the limit for certain scenarios.
    # ransack doesn't return a list if limit is 1 so change to 2.
    # also, default to 50 if limit is not set.
    def set_limit!
      @params[:limit] = case @params[:limit]
                        when '0'
                          @params.delete(:limit)
                        when '1'
                          '2'
                        when nil, ''
                          '50'
                        else
                          @params[:limit]
                        end
    end

    # cant use group scope with postgres
    def distinctify!
      return unless @words.length > 1
      ds =
        @params[:group] ? @params[:group].select { |_k, v| v == '1' }.keys : []
      ds.each do |d|
        @words = @words.group_by { |word| word[d] }.values.map(&:first)
      end
    end
  end
end
