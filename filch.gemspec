lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'filch/version'

Gem::Specification.new do |spec|
  spec.name          = 'filch'
  spec.version       = Filch::VERSION
  spec.authors       = ['ynweber']
  spec.email         = ['foo']

  spec.summary       = %(Basicly just a form builder.)
  spec.description   = %(Creates search forms based on templates configurable in model. Using Ransack.)
  spec.homepage      = 'https://bitbucket.org/ynweber/filch/'
  spec.license       = 'MIT'


  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/ynweber/filch/src'
  spec.metadata['changelog_uri'] = 'http://www.google.com'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0")
                     .reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'haml'
  spec.add_dependency 'ransack'
  # spec.add_dependency 'js_weber'
  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'minitest-rails'
  spec.add_development_dependency 'minitest-reporters', '>= 1.1'
  spec.add_development_dependency 'rake', '~> 10.0'
end
