class FilchController < ::ApplicationController
  def datalist
    model, column, pq = params[:column].split('.')
    @q = Filch.datalist(model, column, params[pq])
    render(
      partial: 'filch/quick',
      layout: false,
      locals: { q: @q }
    )
  end
end
