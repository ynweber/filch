require 'test_helper'

class FilchTest < Minitest::Test
  def setup
    adv = Filch::Templates
    @adv = adv.new(nil)
  end

  def test_that_it_has_a_version_number
    refute_nil ::Filch::VERSION
  end

  # def test_it_does_something_useful
  #   assert false
  # end

  def test_templates_exist
    refute_nil @adv.templates
  end

  def test_templates_is_hash
    assert_kind_of Hash, @adv.templates
  end

  def test_template_is_array
    assert_kind_of Array, @adv.template_keys
  end

  def test_assocs_is_a_hash
    assert_kind_of Hash, @adv.templates['blah'].associations
  end

  def test_assocs_keys_is_array
    assert_kind_of Array, @adv.templates['blah'].associations_keys
  end

  def test_attrs_is_a_hash
    assert_kind_of Hash, @adv.templates['blah'].attributes
  end

  def test_attr_is_array
    assert_kind_of Array, @adv.templates['blah'].attributes['bloo']
  end
end
