$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'minitest/autorun'
require 'rails/generators'
require 'rails/engine'
require 'minitest/rails'
require 'minitest/spec'
require 'minitest/reporters'
require 'filch'

Minitest::Reporters.use!
